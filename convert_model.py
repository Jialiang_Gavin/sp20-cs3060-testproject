#convert model from GPU to CPU compatible format
# Usage: python convert_model.py --weights path/to/best.pt --output path/to/best_cpu.pt


import torch
import pathlib
import argparse

def convert_model(weights_path, output_path):
    try:
        print(f"Loading model from: {weights_path}")
        
        # Ensure compatibility for systems where PosixPath causes issues
        temp = pathlib.PosixPath
        pathlib.PosixPath = pathlib.WindowsPath

        # Load the model
        model = torch.load(weights_path, map_location=torch.device('cpu'))
        print("Model loaded successfully.")

        # Save the model to a new file
        torch.save(model, output_path)
        print(f"Model re-saved to: {output_path}")

        # Revert pathlib change to avoid side effects
        pathlib.PosixPath = temp

    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert YOLOv5 model trained on GPU to CPU-compatible format.")
    parser.add_argument("--weights", type=str, required=True, help="Path to the original .pt file")
    parser.add_argument("--output", type=str, default="best_cpu.pt", help="Path to save the converted .pt file")
    args = parser.parse_args()

    convert_model(args.weights, args.output)
